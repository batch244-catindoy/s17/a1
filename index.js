/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function sampleInfo(){
		let name = prompt("What is your name?");
		let age = prompt("How old are you?");
		let address = prompt("Where do you leave?");
		console.log("Hello, " + name);
		console.log("You are " + age + " years old.");
		console.log("You live in " + address);
		alert("Thank you for your input");
	}
	sampleInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function favBands(){
		console.log ("1. My Chemical Romance");
		console.log ("2. Red Jumpsuit Apparatus");
		console.log ("3. Fall Out Boy");
		console.log ("4. Paramore");
		console.log ("5. Linkin Park");
	}
	favBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	// let printBands = function 
	
	//third function here:

	function favMovies(){
		let movie1 = "John Wick";
		let movie2 = "John Wick:Chapter 2";
		let movie3 = "John Wick:Chapter 3";
		let movie4 = "Deadpool";
		let movie5 = "Law Abiding Citizen";
		console.log('1. ' +  movie1);
		console.log("Rotten Tomatoes Rating 86%")
		console.log('2. ' +  movie2);
		console.log("Rotten Tomatoes Rating 89%")
		console.log('3. ' +  movie3);
		console.log("Rotten Tomatoes Rating 89%")
		console.log('4. ' +  movie4);
		console.log("Rotten Tomatoes Rating 85%")
		console.log('5. ' +  movie5);
		console.log("Rotten Tomatoes Rating 75%")
	}
	favMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


	alert("Hi! Please add the names of your friends.");
	let printFriends = function(){
	
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");
	
	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

 printFriends();
 // console.log(friend1);
 // console.log(friend2);